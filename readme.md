Atelier de découverte Overpass API
===================================

Cet atelier a été dispensé le 10 juillet 2015 à [UDOS](http://www.udos.fr) par Julien Fastré.

Il couvre les informations suivantes : 

- découverte du langage QL, utilisé par [Overpass API](http://overpass-api.de);
- architecture overpass api/ [overpass turbo](http://overpass-turbo.eu) ;
- rappel du modèle de donnée [d'OpenStreetMap](http://openstreetmap.org) et correspondance avec le modèle utilisé par Overpass api.

L'atelier introduit les concepts du langage, et propose ensuite des requêtes qui les mettent en pratique. En fonction du niveau des utilisateurs, il est conseillé au formateur de préparer les requêtes proposées en établissant le pseudo-code avec eux, et/ou en leur proposant d'écrire les requêtes par petits groupes.

Licence
--------

Cet atelier est disponible sous licence CC-BY-SA 4.0 par [Champs Libres Cooperative SCRLFS](http://champs-libres.coop).

