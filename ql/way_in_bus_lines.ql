[out:json][timeout:25];

// on récupère l'aire de digne-les-bains
{{geocodeArea:"Digne-Les-Bains"}}->.digne;

// on filtre les relations type=route et route=bus dans l'aire de digne
rel[type=route][route=bus](area.digne)->.bus_lines;

// on affiche un premier comptage des lignes de bus
.bus_lines out count;

// on filtre les chemins sur ces lignes
way(r.bus_lines)->.ways_bus;

// on filtre uniquement les chemins dans ces lignes
way.ways_bus(area.digne)->.ways_bus_digne;

// on compte les chemins dans digne
.ways_bus_digne out count;
// on affiche les tags des lignes de bus
.bus_lines out tags;
// on affiche les chemins
.ways_bus_digne out skel;
// et les points enfants
.ways_bus_digne >;
out skel;

