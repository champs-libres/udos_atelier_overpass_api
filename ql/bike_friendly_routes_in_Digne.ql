[out:json][timeout:25];

//trouver l'area "Digne-les_bains"
{{geocodeArea:"Digne-Les-Bains"}}->.digne;

//trouver les chemins
(
  way["highway"](area.digne);
)->.highways;

//filtrer sur aménagements
(
  // rues qui ont une pistes cyclable
  way.highways["cycleway"];
  // rues dédiées au vélo
  way.highways["highway"="cycleway"];
  // rues avec pistes cyclable à gauche ou à droite
  way.highways["cycleway:left"];
  way.highways["cycleway:right"];
  // sens uniques limités
  way.highways["oneway:bicycle"="no"];
  way.highways["bicycle:oneway"="no"];
)->.bike_friendly;

.bike_friendly out body;
.bike_friendly>; out skel qt;

