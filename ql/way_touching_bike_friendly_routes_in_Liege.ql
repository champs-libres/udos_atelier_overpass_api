[out:json][timeout:60];

//trouver l'area "Digne-Les-Bains"
{{geocodeArea:"Digne-Les-Bains"}}->.digne;

//trouver les chemins
(
  way["highway"](area.digne);
)->.highways;

//filtrer sur aménagements
(
  // rues qui ont une pistes cyclable
  way.highways["cycleway"];
  // rues dédiées au vélo
  way.highways["highway"="cycleway"];
  // rues avec pistes cyclable à gauche ou à droite
  way.highways["cycleway:left"];
  way.highways["cycleway:right"];
  // sens uniques limités
  way.highways["oneway:bicycle"="no"];
  way.highways["bicycle:oneway"="no"];
)->.bike_friendly;

node(w.bike_friendly)->.points;

way(bn.points)->.all_ways_touched_by_bike_friendly;

(
  way.all_ways_touched_by_bike_friendly[highway] 
  -   
  way.bike_friendly
)->.way_crossing_bike_friendly;

.way_crossing_bike_friendly out skel qt;

.way_crossing_bike_friendly>;

out skel qt ;
