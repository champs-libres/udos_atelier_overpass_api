[out:json][timeout:25];

//trouver l'area "Digne-Les-Bains"
{{geocodeArea:"Digne-Les-Bains"}}->.digne;

//trouver les banques
(
  way[amenity=bank](area.digne);
  node[amenity=bank](area.digne);
)->.banks;

//filtrer sur atm
(
  way.banks[atm=yes];
  node.banks[atm=yes];
)->.banks_with_atm;

// NOTE: il est possible de combiner les filtres :
//(
//  way[amenity=bank][atm=yes](area.digne);
//  node[amenity=bank][atm=yes](area.digne);
//)->.banks_with_atm;

.banks_with_atm out body;
.banks_with_atm>; out skel qt;

