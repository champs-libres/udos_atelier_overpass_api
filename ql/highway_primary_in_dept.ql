[out:json][timeout:25];

//trouver l'area "Alpes-de-haute-Provence"
{{geocodeArea:"Alpes-de-Haute-Provence"}}->.dept;

//filtrer les routes principales
way["highway"="primary"](area.dept)->.routes;

//filtrer les routes dont la ref. commence par D
way.routes["ref"~"^D"];

out body;
>;
out skel qt;
