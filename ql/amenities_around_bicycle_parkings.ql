[out:json][timeout:25];

{{geocodeArea:"Digne-Les-Bains"}}->.digne;

(
  node["amenity"="bicycle_parking"](area.digne);
  way["amenity"="bicycle_parking"](area.digne);
  )->.parkings;

(
  node(around.parkings:50)["amenity"];
  way(around.parkings:50)["amenity"];
 );


out body;

>;
out skel qt;
