[out:json][timeout:25];

//trouver l'area "Digne-Les-Bains"
{{geocodeArea:"Digne-Les-Bains"}}->.digne;

//trouver les amenities;
(
  way["amenity"](area.digne);
  node["amenity"](area.digne);
)->.amenities;

(
  way.amenities["wheelchair"="yes"];
  node.amenities["wheelchair"="yes"];
)->.accessibles;

.accessibles out body;
.accessibles>; 
out skel qt;

